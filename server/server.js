const express = require('express');
const path = require("path")
const { port } = require('./config');

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(express.static('./public/static'));
app.use(express.static('./public/dist'));

app.get("*", (req, res) => {
	res.sendFile(path.resolve('./public/index.html'));
});

app.listen(port, () => console.log(`App listening on http://localhost:${port}!`));
