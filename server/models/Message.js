const mongoose = require('mongoose');
const User = require("./User");

const Message = new mongoose.Schema({
	text: String,
	user: User
});

module.exports = mongoose.model('Message', Message);
