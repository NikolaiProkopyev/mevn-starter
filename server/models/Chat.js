const mongoose = require('mongoose');
const User = require("./User");
const Message = require("./Message");

const Chat = new mongoose.Schema({
	title: String,
	desc: String,
	image: String,
	owner: User,
	users: [User],
	messages: [Message]
});

module.exports = mongoose.model('Chat', Chat);
